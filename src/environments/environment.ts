// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  _firebase: {
    apiKey: "AIzaSyALvqgBfd-MTOKIBhD4pg0PG4YmKSZJU4w",
    authDomain: "petshop-98e5f.firebaseapp.com",
    databaseURL: "https://petshop-98e5f.firebaseio.com",
    projectId: "petshop-98e5f",
    storageBucket: "petshop-98e5f.appspot.com",
    messagingSenderId: "369249008182",
    appId: "1:369249008182:web:96330fe3bafee343"
  },
  get firebase() {
    return this._firebase;
  },
  set firebase(value) {
    this._firebase = value;
  },

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
