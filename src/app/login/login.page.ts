import { Router } from '@angular/router';
import { HomePage } from './../../../petshop/src/app/home/home.page';
import { Component, OnInit } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth'
import {auth} from 'firebase/app'
import { NavController } from '@ionic/angular';

@Component({
  
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
  
})
export class LoginPage implements OnInit {
  loginTxt:string ="" 
  senhaTxt:string ="" 
   
  constructor(public afAuth: AngularFireAuth,public navCtrl: NavController, public router: Router
    ) { }
 
   
  ngOnInit() {
  }

  
  homePage: HomePage;
 async logar( ){
      const {loginTxt, senhaTxt} = this
      try{
        const res = await this.afAuth.auth.signInWithEmailAndPassword(loginTxt , senhaTxt )
        if (res.user!=null){
          this.router.navigateByUrl('/home');
        }


      }catch(err){
        
        console.dir(err)
        if(err.code === "auth/user-not-found"){
          console.log("User not found")
        }
      }
    
    }
   
  }

