import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  public appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'Perfil',
      url: '/perfil',
      icon: 'contact'
    },
    {
      title: 'Consutas',
      url: '/consultas',
      icon: 'search'
    },
    {
      title: 'Agendamento',
      url: '/agendamento',
      icon: 'calendar'
    },
    {
      title: 'Cadastro Cliente',
      url: '/cadastro',
      icon: 'person-add'
    },
    {
      title: 'Cadastro Animal',
      url: '/cadastro-pet',
      icon: 'log-out'
    },
    {
      title: 'Sair',
      url: '/list',
      icon: 'log-out'
    }
  ];


  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public router: Router
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
