import { Component, OnInit } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth'
import {auth} from 'firebase/app'
import {AlertController}from '@ionic/angular'
import { Router } from '@angular/router';

@Component({
  selector: 'app-registre-se',
  templateUrl: './registre-se.page.html',
  styleUrls: ['./registre-se.page.scss'],
})
export class RegistreSePage implements OnInit {
  loginTxt: string=""
  senhaTxt: string=""
  csenhaTxt: string=""
  

  constructor(
    public afAuth: AngularFireAuth,
    public alert: AlertController,
    public router: Router
    ) { }

  ngOnInit() {
  }

  async registre(){
     const{loginTxt, senhaTxt, csenhaTxt} = this
     if(senhaTxt !== csenhaTxt){
       this.showAlert("Erro!", "Senha não confere")
      return console.error("Senha não confere")
}
       
     try{
       
      const res = await this.afAuth.auth.createUserWithEmailAndPassword(loginTxt, senhaTxt )
      console.log(res)
      this.showAlert("Sucesso!", "Bem Vindo") 
      this.router.navigate(['/login'])
     }catch(error){
        console.dir(error)
        console.log(loginTxt, senhaTxt)
        this.showAlert("Erro", error.message)
     
     }
  }
  async showAlert(header: string, message: string){
    const alert = await this.alert.create({
      header,
      message,
      buttons: ["OK"]
    })
    await alert.present()
  }

}
