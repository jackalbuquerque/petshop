import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: './home/home.module#HomePageModule'
  },
 
  { path: 'perfil',
   loadChildren: './perfil/perfil.module#PerfilPageModule' 
  },
  { path: 'consultas', 
  loadChildren: './consultas/consultas.module#ConsultasPageModule' 
},
  { path: 'agendamento', 
  loadChildren: './agendamento/agendamento.module#AgendamentoPageModule' 
},
  { path: 'cadastro', 
  loadChildren: './cadastro/cadastro.module#CadastroPageModule' 
},
  { path: 'login', 
  loadChildren: './login/login.module#LoginPageModule' 
},
  { path: 'recsenha', loadChildren: './recsenha/recsenha.module#RecsenhaPageModule' },
  { path: 'registre-se', loadChildren: './registre-se/registre-se.module#RegistreSePageModule' },  { path: 'cadastro-pet', loadChildren: './cadastro-pet/cadastro-pet.module#CadastroPetPageModule' }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
